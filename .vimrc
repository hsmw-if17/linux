" Zeilennummern aktivieren
set number

" Einrückbreite auf 4 anstatt 8 Leerzeichen stellen
" außerdem Tabs anstatt Leerzeichen verwenden
set tabstop=4
set shiftwidth=4
set softtabstop=0 noexpandtab

" automatisch einrücken
set autoindent

" syntax einfärbung einschalten
syntax on

" vi-kompatibilität ausschalten
set nocompatible
