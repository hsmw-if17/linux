#!/bin/bash

if [ $# -lt 2 ] #wenn weniger als 2 parameter
then
	echo "usage: treegrep '[options] pattern' 'file(s)' [directory]"
	echo "omitting directory searches in current working directory"
	exit 1
fi

regEx=$1 #regulärer ausdruck ist parameter 1
dir=${3:-`pwd`} #nutze parameter 3, oder falls leer `pwd`

for name in `find $dir -type d -print` #für jedes verzeichnis im angegebenen verzeichnis...
do
	grep $regEx $name/$2 #durchsuche datei $2 in verzeichnis nach $regEx
	#falls $2 muster wie "*.txt" dann durchsuche alle dateien die auf dieses muster passen
done
#echo "treegrep finished"
