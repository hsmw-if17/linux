# LINUX.IF17wi
Hier findet ihr Lösungen (Shellscripts), Hilfen, etc zu den Linux Praktika.  
Geordnet nach den jeweiligen Praktika (Ordner prak01, etc)  
Oder falls allgemeingültig, direkt im Hauptverzeichnis.  

## Erklärung zur "Lizenz"
Alle in dieser Repository enthaltenen Dateien sind euch unter der MIT-License zur Verfügung gestellt.   
Die MIT-License ist in der Datei "LICENSE" nachlesbar.  
Im Grunde ermöglicht euch diese, die hier enthaltenen Dateien zu jedem beliebigen Zwecke einzusetzen und beliebig zu ändern und weiterzugeben ohne den Urheberrechtsinhaber (mich) um Erlaubnis zu bitten, solange ihr eine Kopie der Lizenz mit anfügt.  
Es besteht allerdings keinerlei Garantie oder Haftung.
